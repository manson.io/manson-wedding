app:
	@go build -o bin/webserver cmd/webserver/main.go

css-prod:
	@npx tailwindcss -i ./pkg/rendering/css/main.css -o ./assets/css/main.css --minify

css:
	@npx tailwindcss -i ./pkg/rendering/css/main.css -o ./assets/css/main.css

css-watch:
	@npx tailwindcss -i ./pkg/rendering/css/main.css -o ./assets/css/main.css --watch

run:
	@./bin/webserver

dev:
	@~/go/bin/air

build: css app
