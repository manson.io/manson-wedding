# Build the tailwind css files
FROM node:18-bullseye as css-build
WORKDIR /srv
COPY . .
RUN make css-prod

# Build the golang binary
FROM golang:alpine as go-build
WORKDIR /srv
# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
COPY go.mod go.sum ./
RUN go mod download && go mod verify
COPY --from=css-build /srv .
ENV GOOS=linux
ENV GOARCH=amd64
RUN go build -v -o /usr/local/bin/app ./cmd/...

# Copy and run the golang binary
FROM alpine:latest as server
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=go-build /usr/local/bin/app ./
EXPOSE 3000
CMD ["./app"]
