package router

import (
	"encoding/json"
	"html/template"
	"io/ioutil"
	"log"
	"manson-wedding/assets"
	"manson-wedding/pkg/http/router/routes"
	"manson-wedding/pkg/rsvp"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type storage interface {
	AddRsvp(values []string) error
}

var nav = []routes.NavEntry{
	{Text: "Home", Href: "#", Template: "home.html"},
	{Text: "The Wedding", Href: "#wedding", Template: "wedding.html"},
	{Text: "RSVP", Href: "#rsvp", Template: "rsvp.html"},
	{Text: "Accommodation", Href: "#accommodation", Template: "accommodation.html"},
	{Text: "Registry", Href: "#registry", Template: "registry.html"},
	{Text: "Gallery", Href: "#gallery", Template: "gallery.html"},
	{Text: "Contact Us", Href: "#contact-us", Template: "contact.html"},
}

// When dependencies exist, pass them into handler. e.g. services
func Handler(t *template.Template, s rsvp.Service, basePath string) http.Handler {
	// Create the router
	router := httprouter.New()

	//======
	// Bind the base routes
	//======

	// Health check
	router.GET("/health", routes.Health())

	// Static files
	router.ServeFiles("/static/*filepath", http.FS(assets.Assets))

	// Redirect paths with missing base to main web routes
	router.GET("/", redirectMain())
	router.GET("/so", redirectMain())

	// HTML pages
	router.GET(basePath, page(t, false))
	router.GET(basePath+"/so", page(t, true))
	// for i := range nav {
	// 	router.GET(nav[i].Href, routes.Page(t, routes.PageData{Nav: nav, NavIndex: i, Data: map[string]any{}}))
	// }

	router.POST(basePath+"/rsvp", addRsvp(s))

	return router
}

type pageData struct {
	Nav           []routes.NavEntry
	AllowSoInvite bool
}

func redirectMain() func(_writer http.ResponseWriter, _request *http.Request, _ httprouter.Params) {
	return func(_writer http.ResponseWriter, _request *http.Request, _ httprouter.Params) {
		http.Redirect(_writer, _request, "/natasza-and-mark"+_request.URL.Path, http.StatusMovedPermanently)
	}
}

func page(t *template.Template, allowSoInvite bool) func(_writer http.ResponseWriter, _request *http.Request, _ httprouter.Params) {
	return func(_writer http.ResponseWriter, _request *http.Request, _ httprouter.Params) {
		_writer.Header().Set("Content-Type", "text/html")
		err := t.ExecuteTemplate(_writer, "index.html", pageData{Nav: nav, AllowSoInvite: allowSoInvite})
		if err != nil {
			log.Fatal(err)
		}
	}
}

func addRsvp(s rsvp.Service) func(_writer http.ResponseWriter, _request *http.Request, _ httprouter.Params) {
	return func(_writer http.ResponseWriter, _request *http.Request, _ httprouter.Params) {
		body, err := ioutil.ReadAll(_request.Body)
		if err != nil {
			log.Println("error reading post body")
			log.Println(err)
			routes.JsonResponse(
				_writer,
				map[string]string{
					"error": "could not parse json",
				},
				http.StatusBadRequest,
			)
			return
		}
		rsvpRawData := map[string]map[string]any{}
		err = json.Unmarshal(body, &rsvpRawData)
		if err != nil {
			if e, ok := err.(*json.SyntaxError); ok {
				log.Println(string(body))
				log.Printf("syntax error at byte offset %d", e.Offset)
			}
			log.Println("error parsing json")
			log.Println(err)
			routes.JsonResponse(
				_writer,
				map[string]string{
					"error": "could not parse json",
				},
				http.StatusBadRequest,
			)
			return
		}

		soAttendingWedding := rsvpRawData["so"]["attending_ceremony"]
		soAttendingParty := rsvpRawData["so"]["attending_party"]
		isSoAttending := soAttendingWedding.(string) == "true" || soAttendingParty.(string) == "true"
		soAttending := "N"
		if isSoAttending {
			soAttending = getStringFromInput(rsvpRawData["so"]["name"])
		}

		guestDietaryRequirements := getStringFromInput(rsvpRawData["guest"]["dietary_requirements"])
		if guestDietaryRequirements == "other" {
			guestDietaryRequirements = getStringFromInput(rsvpRawData["guest"]["dietary_requirements_text"])
		}
		soDietaryRequirements := getStringFromInput(rsvpRawData["so"]["dietary_requirements"])
		if soDietaryRequirements == "other" {
			soDietaryRequirements = getStringFromInput(rsvpRawData["so"]["dietary_requirements_text"])
		}

		guestRsvp := rsvp.Rsvp{
			Name:                getStringFromInput(rsvpRawData["guest"]["name"]),
			AttendingWedding:    getStringFromInput(rsvpRawData["guest"]["attending_ceremony"]),
			AttendingParty:      getStringFromInput(rsvpRawData["guest"]["attending_party"]),
			DietaryRequirements: guestDietaryRequirements,
			HasAllergies:        getStringFromInput(rsvpRawData["guest"]["has_allergies"]),
			Allergies:           getStringFromInput(rsvpRawData["guest"]["allergies"]),
			EmailAddress:        getStringFromInput(rsvpRawData["guest"]["email_address"]),
			PhoneNumber:         getStringFromInput(rsvpRawData["guest"]["phone_number"]),
			MusicRequest:        getStringFromInput(rsvpRawData["composite"]["song_request"]),
			ParkingRequired:     getStringFromInput(rsvpRawData["composite"]["parking_space_required"]),
			SoAttending:         soAttending,
		}

		soRsvp := rsvp.Rsvp{
			Name:                getStringFromInput(rsvpRawData["so"]["name"]),
			AttendingWedding:    getStringFromInput(rsvpRawData["so"]["attending_ceremony"]),
			AttendingParty:      getStringFromInput(rsvpRawData["so"]["attending_party"]),
			DietaryRequirements: soDietaryRequirements,
			HasAllergies:        getStringFromInput(rsvpRawData["so"]["has_allergies"]),
			Allergies:           getStringFromInput(rsvpRawData["so"]["allergies"]),
			EmailAddress:        getStringFromInput(rsvpRawData["so"]["email_address"]),
			PhoneNumber:         getStringFromInput(rsvpRawData["so"]["phone_number"]),
			MusicRequest:        "",
			ParkingRequired:     "",
			SoAttending:         "",
		}

		err = s.AddRsvp(guestRsvp, soRsvp)
		if err != nil {
			log.Println(err)
			routes.JsonResponse(
				_writer,
				map[string]string{
					"error": "an error occurred",
				},
				http.StatusInternalServerError,
			)

			return
		}

		routes.JsonResponse(_writer, map[string]string{}, http.StatusCreated)
	}
}

func getStringFromInput(value any) string {
	if value == nil {
		return ""
	}
	return value.(string)
}
