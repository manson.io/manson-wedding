package routes

import (
	"html/template"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type PageData struct {
	Nav      []NavEntry
	NavIndex int
	Data     map[string]any
}

func Page(t *template.Template, pd PageData) func(_writer http.ResponseWriter, _request *http.Request, _ httprouter.Params) {
	return func(_writer http.ResponseWriter, _request *http.Request, _ httprouter.Params) {
		_writer.Header().Set("Content-Type", "text/html")
		err := t.ExecuteTemplate(_writer, pd.Nav[pd.NavIndex].Template, pd)
		if err != nil {
			log.Fatal(err)
		}
	}
}
