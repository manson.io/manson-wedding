package routes

type NavEntry struct {
	Text string
	Href string
	Template string
}
