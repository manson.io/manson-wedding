package routes

import (
	"encoding/json"
	"log"
	"net/http"
)

// Outputs a struct as an HTTP json response
func JsonResponse(_writer http.ResponseWriter, data any, statusCode int) {
	jsonResponse, err := json.Marshal(data)

	if err != nil {
		log.Fatal("Could not marshal json response.")
	}

	_writer.Header().Set("Content-Type", "application/json")
	_writer.WriteHeader(statusCode)
	_writer.Write(jsonResponse)
}
