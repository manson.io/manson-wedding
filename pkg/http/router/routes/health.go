package routes

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func Health() func(_writer http.ResponseWriter, _request *http.Request, _ httprouter.Params) {
	return func(_writer http.ResponseWriter, _request *http.Request, _ httprouter.Params) {
		_writer.Header().Set("Content-Type", "application/json")
		JsonResponse(_writer, &map[string]any{
			"services": &map[string]any{
				"router": "ok",
			},
		}, http.StatusCreated)
	}
}
