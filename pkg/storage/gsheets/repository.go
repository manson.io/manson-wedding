package gsheets

import (
	"context"
	"log"

	"google.golang.org/api/option"
	"google.golang.org/api/sheets/v4"
)

type repository struct {
	client  *sheets.Service
	sheetId string
}

func getClient(serviceAccountFilePath string) (*sheets.Service, error) {
	ctx := context.Background()
	return sheets.NewService(ctx, option.WithCredentialsFile(serviceAccountFilePath))
}

func NewRepository(serviceAccountFilePath string, sheetId string) (repository, error) {
	client, err := getClient(serviceAccountFilePath)

	if err != nil {
		return repository{}, err
	}

	return repository{
		client:  client,
		sheetId: sheetId,
	}, nil
}

func (_r repository) AddRsvp(_values []string) error {
	r := "Sheet1!1:9999"
	v := make([]any, len(_values))

	for i, _v := range _values {
		v[i] = _v
	}
	log.Println(v)
	_, err := _r.client.Spreadsheets.Values.Append(
		_r.sheetId,
		r,
		&sheets.ValueRange{Values: [][]any{v}, MajorDimension: "ROWS"},
	).ValueInputOption("RAW").Do()

	return err
}
