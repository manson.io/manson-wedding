package rsvp

type repository interface {
	AddRsvp(values []string) error
}

type Service struct {
	r repository
}

type Rsvp struct {
	Name                string
	AttendingWedding    string
	AttendingParty      string
	DietaryRequirements string
	HasAllergies        string
	Allergies           string
	MusicRequest        string
	ParkingRequired     string
	EmailAddress        string
	PhoneNumber         string
	SoAttending         string // Y/N/guest-name
}

func NewService(r repository) Service {
	return Service{
		r: r,
	}
}

func (s Service) AddRsvp(guestRsvp Rsvp, soRsvp Rsvp) error {
	// TODO: validation here
	err := s.r.AddRsvp(guestRsvp.toStringArray(false))
	if err != nil {
		return err
	}

	if soRsvp.AttendingParty == "true" || soRsvp.AttendingWedding == "true" {
		err = s.r.AddRsvp(soRsvp.toStringArray(true))
		if err != nil {
			return err
		}
	}

	return nil
}

func (rsvp Rsvp) toStringArray(isSo bool) []string {
	strings := []string{}
	strings = append(strings, rsvp.Name)
	strings = append(strings, convertHumanReadableBoolText(rsvp.AttendingWedding))
	strings = append(strings, convertHumanReadableBoolText(rsvp.AttendingParty))
	strings = append(strings, rsvp.DietaryRequirements)
	strings = append(strings, convertHumanReadableBoolText(rsvp.HasAllergies))
	strings = append(strings, rsvp.Allergies)
	strings = append(strings, convertHumanReadableBoolText(rsvp.SoAttending))
	strings = append(strings, rsvp.MusicRequest)
	strings = append(strings, convertHumanReadableBoolText(rsvp.ParkingRequired))
	strings = append(strings, rsvp.EmailAddress)
	strings = append(strings, rsvp.PhoneNumber)

	return strings
}

func convertHumanReadableBoolText(text string) string {
	if text == "true" {
		return "yes"
	}

	if text == "false" {
		return "no"
	}

	if text == "Y" {
		return "yes"
	}

	if text == "N" {
		return "no"
	}

	return text
}
