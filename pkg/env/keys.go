package env

import (
	"github.com/joho/godotenv"
)

const (
	SHEETS_API_SHEET_ID string = "SHEETS_API_SHEET_ID"
	WEB_PORT            string = "WEB_PORT"
)

func LoadEnv() error {
	return godotenv.Load(".env")
}
