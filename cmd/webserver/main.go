package main

import (
	"fmt"
	"html/template"
	"log"
	"manson-wedding/pkg/env"
	"manson-wedding/pkg/http/router"
	"manson-wedding/pkg/rendering"
	"manson-wedding/pkg/rsvp"
	"manson-wedding/pkg/storage/gsheets"
	"net/http"
	"os"
)

func main() {
	// Load the environment variables from the .env file
	err := env.LoadEnv()
	if err != nil {
		log.Println("Could not load .env file: " + err.Error() + ". Using host environment variables.")
	}

	// Parse templates
	t, err := template.ParseFS(rendering.Templates, "templates/pages/*.html", "templates/partials/*.html", "templates/sections/*.html")
	if err != nil {
		log.Fatal(err)
	}

	// Get working directory
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	// Create storage system
	storage, err := gsheets.NewRepository(wd+"/google-service-credentials.json", os.Getenv(env.SHEETS_API_SHEET_ID))
	if err != nil {
		log.Println(err)
	}

	// Create RSVP service
	rsvpService := rsvp.NewService(storage)

	// Set up routes
	router := router.Handler(t, rsvpService, "/natasza-and-mark")

	// Set host port
	port := os.Getenv(env.WEB_PORT)
	if port == "" {
		port = "3000"
	}

	// Serve
	fmt.Println("Server online at http://localhost:" + port)
	log.Fatal(http.ListenAndServe(":"+port, router))
}
